# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve 


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # -----
    # solve
    # -----

    #def test_solve(self):
        #self.assertEqual(1, 1)
    
    def test_solve_1(self):
        # a test case in the project description
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        # testing alphabet ordering of the output and ties
        r = StringIO("D Beijing Support A\nB Kyiv Hold\nF Taipei Move Beijing\n"+
                     "E London Support B\nC DC Support B\nA Moscow Move Kyiv\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Kyiv\nC DC\nD [dead]\nE London\nF [dead]\n")
        
    def test_solve_3(self):
        # testing the cyclic movement of the armies
        r = StringIO("A Madrid Move Barcelona\nC RiodeJaneiro Move SaoPaolo\n"+
                     "D SaoPaolo Move Madrid\nB Barcelona Move RiodeJaneiro\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB RiodeJaneiro\nC SaoPaolo\nD Madrid\n")    


# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
